## Ejercicio 4 - Strings y arrays

Crea una función llamada modificaPares que reciba dos argumentos: un array y una función anónima.

La función debe devolver un nuevo array a partir del primero, en el que sus elementos en las posiciones pares han sido modificados por la función recibida como segundo argumento.

Por ejemplo:

```javascript
let palabras = ['café', 'croissant', 'tostada', 'zumo'];
let palabras2 = modificaPares(palabras, elemento => capitaliza(elemento));
console.log(palabras2);
```

Imprimirá:

```
['café', 'Croissant', 'tostada', 'Zumo']
```